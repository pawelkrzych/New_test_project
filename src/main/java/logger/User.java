package logger;

import java.time.LocalDate;

public class User {

    private int id;
    private String Name;
    private String Surname;
    private String login;

    public void setName(String name) {
        Name = name;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    private String password;
    private String email;
    private LocalDate dateOfBirth;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
